package com.framework;

import org.junit.Test;

import com.framework.util.generator.GenerateFactory;
import com.framework.util.generator.util.FileType;

public class Generator {

	@Test
	public void generate() {
		// new GenerateFactory().genJavaTemplate(FileType.SERVICE, FileType.ENTITY, FileType.EXAMPLE, FileType.JAVA_DAO,
		// FileType.XML_DAO);
		// new GenerateFactory().genJavaTemplate(FileType.XML_DAO);
	}

	@Test
	public void generateAll() {
		new GenerateFactory().genJavaTemplate(FileType.SERVICE, FileType.ENTITY, FileType.EXAMPLE, FileType.JAVA_DAO,
				FileType.XML_DAO);
	}

}
