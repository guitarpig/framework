package com.framework.data;

import java.util.Arrays;

import com.framework.entity.admin.system.Department;
import com.framework.entity.admin.system.User;
import com.framework.util.Clock;
import com.framework.util.Digests;
import com.framework.util.Encodes;

public class GenerateData {

	private static final int SALT_SIZE = 8;

	public static User randomNewUser() {
		User user = new User();
		user.setUserName(RandomData.randomName("user"));
		user.setRealName(RandomData.randomName("User"));
		user.setPlainPassword("123456");
		user.setMobile("158" + RandomData.randomNumber(8));
		user.setEmail(RandomData.randomNumber(6) + "@qq.com");
		user.setGender(RandomData.randomSome(Arrays.asList(new Boolean[] { true, false }), 1).get(0));
		user.setLastVisit(Clock.DEFAULT.getCurrentDate());

		byte[] salt = Digests.generateSalt(SALT_SIZE);
		user.setSalt(Encodes.encodeHex(salt));
		byte[] hashPassword = Digests.sha1(user.getPlainPassword().getBytes(), salt, 1024);
		user.setPassword(Encodes.encodeHex(hashPassword));

		return user;
	}

	public static Department randomNewDepartment() {
		Department department = new Department();
		department.setCode(RandomData.randomNumber(6));
		department.setFullName(RandomData.randomName("部门"));
		department.setShortName(RandomData.randomName("简称"));
		department.setPhone("158" + RandomData.randomNumber(8));
		department.setEmail(RandomData.randomNumber(6) + "@qq.com");
		return department;
	}
}
