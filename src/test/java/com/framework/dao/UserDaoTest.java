package com.framework.dao;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.framework.SpringTransactionalTestCase;
import com.framework.common.Page;
import com.framework.dao.admin.system.UserDao;
import com.framework.data.GenerateData;
import com.framework.entity.admin.system.User;
import com.framework.entity.admin.system.UserExample;
import com.framework.shiro.realm.ShiroDbRealm.ShiroUser;
import com.framework.util.Digests;
import com.framework.util.Encodes;
import com.framework.util.ShiroTestUtils;

/**
 * AbstractTransactionalJUnit4SpringContextTests默认是自动回滚的
 * 测试dao层业务
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月10日 上午11:26:03
 */
@ContextConfiguration(locations = { "/applicationContext.xml" })
@TransactionConfiguration(defaultRollback = false)
public class UserDaoTest extends SpringTransactionalTestCase {

	@Autowired
	private UserDao userDao;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ShiroTestUtils.mockSubject(new ShiroUser(1, "admin", "Admin"));
	}

	@Test
	public void initAdmin() {
		// 删除已存在的超级管理员
		userDao.deleteByPrimaryKey(1);

		User user = new User();
		user.setCode("01");
		user.setUserName("admin");
		user.setPlainPassword("123456");
		user.setDescription("超级管理员");
		user.setEmail("543414648@qq.com");
		user.setGender(Boolean.TRUE);
		user.setRealName("Admin");

		byte[] salt = Digests.generateSalt(8);
		user.setSalt(Encodes.encodeHex(salt));
		byte[] hashPassword = Digests.sha1(user.getPlainPassword().getBytes(), salt, 1024);
		user.setPassword(Encodes.encodeHex(hashPassword));

		assertThat(userDao.insert(user)).isGreaterThan(0);

		// 将新增的用户id改为1
		UserExample example = new UserExample();
		example.createCriteria().andIdEqualTo(user.getId());
		User record = new User();
		record.setId(1);
		assertThat(userDao.updateByExampleSelective(record, example)).isGreaterThan(0);
	}

	@Test
	public void save() {
		User user = GenerateData.randomNewUser();
		assertThat(userDao.insert(user)).isGreaterThan(0);
	}

	@Test
	public void update() {
		User user = userDao.selectByPrimaryKey(1);

		user.setCode("0101");
		userDao.updateByPrimaryKeySelective(user);
	}

	@Test
	public void findByPage() {
		Page page = new Page(6, 2);
		UserExample example = new UserExample();
		example.setPage(page);
		List<User> list = userDao.selectByExample(example);
		for (User user : list) {
			System.out.println(user.getUserName());
		}
	}

	@Test
	public void delete() {
		User user = GenerateData.randomNewUser();
		assertThat(userDao.insert(user)).isGreaterThan(0);
		assertThat(userDao.deleteByPrimaryKey(user.getId())).isGreaterThan(0);
	}
}
