package com.framework.dao;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import com.framework.SpringTransactionalTestCase;
import com.framework.dao.admin.system.DepartmentDao;
import com.framework.data.GenerateData;
import com.framework.entity.admin.system.Department;
import com.framework.entity.admin.system.DepartmentExample;
import com.framework.entity.admin.system.DepartmentExample.Criteria;
import com.framework.shiro.realm.ShiroDbRealm.ShiroUser;
import com.framework.util.ShiroTestUtils;

/**
 * 初始化测试数据
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月10日 上午11:26:03
 */
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class InitDataTest extends SpringTransactionalTestCase {

	@Autowired
	private DepartmentDao departmentDao;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ShiroTestUtils.mockSubject(new ShiroUser(1, "admin", "Admin"));
	}

	@Test
	@Rollback(false)
	public void init() {
		for (int i = 0; i < 100; i++) {
			departmentDao.insert(GenerateData.randomNewDepartment());
		}
	}

	@Test
	public void test() {
		DepartmentExample example = new DepartmentExample();
		Criteria criteria1 = example.createCriteria();
		criteria1.andCodeEqualTo("405269");
		Criteria criteria2 = example.createCriteria();
		example.or(criteria2);

		// example.or().andFullNameEqualTo("部门2730594");
		List<Department> list = departmentDao.selectByExample(example);
	}
}
