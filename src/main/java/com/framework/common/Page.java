package com.framework.common;

import java.io.Serializable;

/**
 * 
 * 分页对象
 * 
 * @Author: lbl
 * @Date: 2015年6月1日 下午3:38:52
 */
public class Page implements Serializable {

	/**
	 * <code>serialVersionUID</code> of comment
	 */
	private static final long serialVersionUID = 1L;

	// 分页查询开始记录位置
	private int start;
	// 分页查看下结束位置
	private int end;
	// 每页显示记录数
	private int rows = 10;
	// 当前页
	private int page = 1;
	// 分页条件
	private String pagingCondition;

	public Page() {
	}

	public Page(int page, int rows) {
		this.page = (page <= 0 ? 1 : page);
		this.rows = (rows <= 0 ? 10 : rows);
	}

	public String getPagingCondition() {
		if (getEnd() > 0) {
			return "limit " + getStart() + "," + getEnd();
		}
		return pagingCondition;
	}

	public void setPagingCondition(String pagingCondition) {
		this.pagingCondition = pagingCondition;
	}

	/**
	 * 拿到开始位置
	 * 
	 * @return
	 */
	public int getStart() {
		this.start = (page - 1) * rows;
		return this.start;
	}

	/**
	 * 拿到结束位置
	 * 
	 * @return
	 */
	public int getEnd() {
		this.end = rows;
		return this.end;
	}

}