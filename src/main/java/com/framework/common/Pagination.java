package com.framework.common;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 分页结果集
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月15日 下午3:34:26
 */
public class Pagination<T> {
	// 表示一页有多少行
	public final static int PAGESIZE = 10;

	private int pageSize = PAGESIZE;
	// 数据结果集
	@JsonIgnore
	private List<T> items;
	// 共多少行
	private int totalCount;
	// 每页开始位置
	private int[] indexes = new int[0];
	// 当前页面索引
	private int currentPageIndex;
	// 开始的位置索引
	private int startIndex = 0;

	public Pagination(List<T> items, int totalCount) {
		setPageSize(PAGESIZE);
		setTotalCount(totalCount);
		setItems(items);
		setStartIndex(0);
		currentPageIndex = getCurrentPageIndex();
	}

	public Pagination(List<T> items, int totalCount, int startIndex) {
		setPageSize(PAGESIZE);
		setTotalCount(totalCount);
		setItems(items);
		setStartIndex(startIndex);
		currentPageIndex = getCurrentPageIndex();
	}

	public Pagination(List<T> items, int totalCount, int pageSize, int startIndex) {
		setPageSize(pageSize);
		setTotalCount(totalCount);
		setItems(items);
		setStartIndex(startIndex);
		currentPageIndex = getCurrentPageIndex();
	}

	/**
	 * 得到已分页好的结果集
	 * 
	 * @return java.util.List
	 */
	public List<T> getItems() {
		return items;
	}

	/**
	 * 设置结果集
	 * 
	 * @param items
	 */
	public void setItems(List<T> items) {
		this.items = items;
	}

	/**
	 * 获得每页的行数
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置每页的行数
	 * 
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 得到总结果数
	 * 
	 * @return int
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * 设置共多少行并计算有多少页
	 * 
	 * @param totalCount
	 */
	public void setTotalCount(int totalCount) {
		if (totalCount > 0) {
			this.totalCount = totalCount;
			int count = totalCount / pageSize;
			if ((totalCount % pageSize) > 0) {
				count++;
			}
			indexes = new int[count];
			// 计算每页开始的位置索引
			for (int i = 0; i < count; i++) {
				indexes[i] = pageSize * i;
			}
		} else {
			this.totalCount = 0;
		}
	}

	/**
	 * 得到分页索引的数组
	 * 
	 * @return int[]
	 */
	public int[] getIndexes() {
		return indexes;
	}

	public void setIndexes(int[] indexes) {
		this.indexes = indexes;
	}

	/**
	 * 当前分页索引
	 * 
	 * @return int
	 */
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * 设置开始的位置
	 * 
	 * @param startIndex
	 */
	public void setStartIndex(int startIndex) {
		if (totalCount <= 0) {
			this.startIndex = 0;
		} else if (startIndex >= totalCount) {
			this.startIndex = indexes[indexes.length - 1];
		} else if (startIndex < 0) {
			this.startIndex = 0;
		} else {
			this.startIndex = indexes[startIndex / pageSize];
		}
	}

	/**
	 * 下一页索引
	 * 
	 * @return int
	 */
	public int getNextIndex() {
		int nextIndex = getStartIndex() + pageSize;
		if (nextIndex >= totalCount) {
			return getStartIndex();
		} else {
			return nextIndex;
		}
	}

	/**
	 * 上一页索引
	 * 
	 * @return int
	 */
	public int getPreviousIndex() {
		int previousIndex = getStartIndex() - pageSize;
		if (previousIndex < 0) {
			return 0;
		} else {
			return previousIndex;
		}
	}

	/**
	 * 获得当前的页面索引
	 * 
	 * @return
	 */
	public int getCurrentPageIndex() {
		return Arrays.binarySearch(this.indexes, this.startIndex);
	}

}