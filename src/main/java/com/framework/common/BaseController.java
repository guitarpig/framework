package com.framework.common;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.WebRequest;

import com.framework.shiro.realm.ShiroDbRealm.ShiroUser;
import com.framework.util.ParamEditor;

public class BaseController {

	/**
	 * 获取当前登陆用户
	 */
	public ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}

	/**
	 * 请求参数绑定处理
	 * 
	 * @param binder
	 * @param request
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		binder.registerCustomEditor(String.class, new ParamEditor());
	}
}
