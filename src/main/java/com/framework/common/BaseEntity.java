package com.framework.common;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 基础字段
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月1日 下午5:43:22
 */
public abstract class BaseEntity {

	// 创建人ID
	protected Integer createUserId;
	// 创建人姓名
	protected String createUserName;
	// 创建时间
	protected Date createTime;
	// 修改人ID
	protected Integer modifyUserId;
	// 修改人姓名
	protected String modifyUserName;
	// 修改时间
	protected Date modifyTime;
	// 有效
	protected Boolean enabled;
	// 删除标记
	protected Boolean deleteMark;
	// 排序
	protected Integer sort;

	/**
	 * @param 设置创建人id
	 */
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return 返回创建人id
	 */
	public Integer getCreateUserId() {
		return this.createUserId;
	}

	/**
	 * @param 设置创建人姓名
	 */
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	/**
	 * @return 返回创建人姓名
	 */
	public String getCreateUserName() {
		return this.createUserName = createUserName == null ? null : createUserName.trim();
	}

	/**
	 * @param 设置创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return 返回创建时间
	 */
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param 设置修改人ID
	 */
	public void setModifyUserId(Integer modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	/**
	 * @return 返回修改人ID
	 */
	public Integer getModifyUserId() {
		return this.modifyUserId;
	}

	/**
	 * @param 设置修改人姓名
	 */
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	/**
	 * @return 返回修改人姓名
	 */
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public String getModifyUserName() {
		return this.modifyUserName = modifyUserName == null ? null : modifyUserName.trim();
	}

	/**
	 * @param 设置修改时间
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	/**
	 * @return 返回修改时间
	 */
	public Date getModifyTime() {
		return this.modifyTime;
	}

	/**
	 * @return 返回有效性
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param 设置有效性
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return 返回删除标记
	 */
	public Boolean getDeleteMark() {
		return deleteMark;
	}

	/**
	 * @param 设置删除标记
	 */
	public void setDeleteMark(Boolean deleteMark) {
		this.deleteMark = deleteMark;
	}

	/**
	 * @return 返回排序
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param 设置排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
