package com.framework.vo;

import java.util.List;

/**
 * 用户jqwidgets分页数据
 * 
 * @Author: guitarpig
 * @Date: 2015年4月13日 上午10:00:50
 */
public class PaginationResult<T> {
	public int pageSize = 20; // 每页多少条
	public int totalRecords; // 总记录数
	public int curPage; // 当前第几页
	public List<T> data; // 当前页数据

	public PaginationResult() {

	}

	public PaginationResult(int totalRecords) {
		super();
		this.totalRecords = totalRecords;
	}

	public PaginationResult(int pageSize, int totalRecords) {
		super();
		this.pageSize = pageSize;
		this.totalRecords = totalRecords;
	}

	public PaginationResult(int pageSize, int totalRecords, int curPage, List<T> data) {
		super();
		this.pageSize = pageSize;
		this.totalRecords = totalRecords;
		this.curPage = curPage;
		this.data = data;
	}

	public PaginationResult(int totalRecords, int curPage, List<T> data) {
		super();
		this.totalRecords = totalRecords;
		this.curPage = curPage;
		this.data = data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

}
