package com.framework.vo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * 返回结果 用于mvc 返回统一的结构的结果对象
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Result<T> implements IResult<T> {

	/**
	 * <code>serialVersionUID</code> of comment
	 */
	private static final long serialVersionUID = 1L;
	// 执行成功
	public static final int SUCCESS = 0;
	// 执行失败
	public static final int FAIL = 1;

	protected T data;
	protected int code;

	public static <T> Result<T> createSuccess() {
		return new Result<T>(SUCCESS);
	}

	public static <T> Result<T> createSuccess(T data) {
		return new Result<T>(data, SUCCESS);
	}

	public static <T> Result<T> createFail() {
		return new Result<T>(FAIL);
	}

	public static <T> Result<T> createFail(T data) {
		return new Result<T>(data, FAIL);
	}

	public static <T> Result<T> create(T data, int code) {
		return new Result<T>(data, code);
	}

	public static <T> Result<T> create(int code) {
		return new Result<T>(code);
	}

	public static <T> Result create(T data) {
		if (data != null) {
			return new Result(data, SUCCESS);
		} else {
			return new Result(data, FAIL);
		}
	}

	private Result(int code) {
		super();
		this.code = code;
	}

	private Result(T data) {
		super();
		this.data = data;
		this.code = SUCCESS;
	}

	private Result(T data, int code) {
		super();
		this.data = data;
		this.code = code;
	}

	@Override
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public boolean success() {
		return this.code == SUCCESS;
	}

}