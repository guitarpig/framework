package com.framework.controller.admin.system;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.common.BaseController;
import com.framework.entity.admin.system.Department;
import com.framework.service.admin.system.DepartmentService;

/**
 * 部门管理
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月11日 下午2:15:10
 */
@Controller
@RequestMapping(value = "/admin/system/department")
public class DeparmentController extends BaseController {

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String index() {
		return "admin/system/department/index";
	}

	@RequestMapping(value = "list", method = RequestMethod.POST)
	@ResponseBody
	public Collection list() {
		return CollectionUtils.select(departmentService.findAll().getData(), new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				Department department = (Department) object;
				return department.getId() < 115;
			}

		});
		// return departmentService.findAll().getData();
	}
}
