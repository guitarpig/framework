package com.framework.controller.admin.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.framework.common.BaseController;
import com.framework.entity.admin.system.DepartmentExample;

/**
 * 部门管理
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月11日 下午2:15:10
 */
@Controller
@RequestMapping(value = "/admin/demo")
public class DemoController extends BaseController {

	@RequestMapping(value = "convertDemo", method = { RequestMethod.POST, RequestMethod.GET })
	public String convertDemo() {
		DepartmentExample example = new DepartmentExample();
		return "admin/system/demo/convertDemo";
	}

	/**
	 * 测试类型转换
	 * 
	 * @return
	 * @Author: lbl 543414648@qq.com
	 * @Date: 2015年6月16日 下午3:06:43
	 */
	@RequestMapping(value = "convertData", method = { RequestMethod.POST, RequestMethod.GET })
	public String convertData() {
		return "admin/system/department/index";
	}

}
