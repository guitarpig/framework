package com.framework.dao.admin.system;

import com.framework.entity.admin.system.User;
import com.framework.entity.admin.system.UserExample;
import com.framework.common.BaseDao;
import com.framework.common.MyBatisRepository;

@MyBatisRepository
public interface UserDao extends BaseDao<User, UserExample, Integer>{
   
}