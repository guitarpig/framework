package com.framework.util;

import java.beans.PropertyEditorSupport;

import org.apache.commons.lang3.StringUtils;

/**
 * 属性编辑器,用于将 request param 转换时清除string 类型字段两端空格回车等字符
 * 
 * @Author: guitarpig
 * @Date: 2015年4月21日 下午4:58:06
 */
public class ParamEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String arg0) throws IllegalArgumentException {
		// System.out.println("arg0 = " + arg0);
		arg0 = StringUtils.trim(arg0);
		super.setValue(arg0);
	}

}
