package com.framework.util.generator;

import com.framework.util.generator.db.DataSource;
import com.framework.util.generator.db.DbFactory;
import com.framework.util.generator.util.ConvertHandler;
import com.framework.util.generator.util.FileType;
import com.framework.util.generator.util.Resources;
import com.framework.util.generator.vo.Table;

public class GenerateFactory {
	private Table table;
	private String tableName;

	public GenerateFactory() {
		this.tableName = Resources.TPL_TABLE_NAME;
	}

	public GenerateFactory(String tableName) {
		this.tableName = tableName;
	}

	public GenerateFactory(Table table) {
		this.table = table;
	}

	private void init() {
		Table table = null;
		try {
			DataSource db = DbFactory.create();
			table = db.getTable(tableName);
			ConvertHandler.tableHandle(table);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		this.table = table;
	}

	public void genJavaTemplate(FileType... fileType) {
		try {
			if (table == null) {
				init();
			}
			if (fileType.length == 0) {
				new GenerateCode(FileType.ENTITY).generate(table);
				new GenerateCode(FileType.EXAMPLE).generate(table);
				new GenerateCode(FileType.XML_DAO).generate(table);
				new GenerateCode(FileType.JAVA_DAO).generate(table);
				new GenerateCode(FileType.SERVICE).generate(table);
				new GenerateCode(FileType.CONTROLLER).generate(table);
			} else {
				for (FileType element : fileType) {
					new GenerateCode(element).generate(table);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
