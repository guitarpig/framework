package com.framework.util.generator.util;

import java.io.File;
import java.util.regex.Matcher;

/**
 * 生成文件的文件类型
 * 
 */
public enum FileType {
	ENTITY("entity", Resources.PKN_ENTITY, Resources.JAVA_ENTITY_TEMPLATE, ".java"), 
	EXAMPLE("example",Resources.PKN_ENTITY, Resources.JAVA_EXAMPLE_TEMPLATE, "Example.java"), 
	JAVA_DAO("java.dao", Resources.PKN_DAO,Resources.JAVA_DAO_TEMPLATE, "Dao.java"), 
	XML_DAO("xml.dao", Resources.PKN_DAO, Resources.XML_DAO_TEMPLATE, "Dao.xml"), 
	SERVICE("service", Resources.PKN_SERVICE, Resources.JAVA_SERVICE_TEMPLATE, "Service.java"), 
	CONTROLLER("controller", Resources.PKN_CONTROLLER, Resources.JAVA_CONTROLLER_TEMPLATE, "Controller.java");

	// 成员变量
	private String type;// 文件类型
	private String pakage;// 包声明
	private String template;// 模板
	private String fileNameExtension;// 文件扩展

	// 构造方法
	private FileType(String type, String pkage, String template, String fileNameExtension) {
		this.type = type;
		this.pakage = pkage;
		this.template = template;
		this.fileNameExtension = fileNameExtension;
	}

	public String getType() {
		return type;
	}

	public String getPakage() {
		return pakage;
	}

	public String getTemplate() {
		return template;
	}

	public String getFileNameExtension() {
		return fileNameExtension;
	}

	/**
	 * 根据Java文件类型获取存储地址
	 * 
	 * @param type
	 * @return
	 */
	public String getJavaStorePath() {
		String packageDecl = getPakage();
		packageDecl = packageDecl.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
		return packageDecl;
	}

	public static void main(String[] args) {
		System.out.println(FileType.ENTITY.getPakage());
		System.out.println(FileType.ENTITY.getTemplate());
	}

}
