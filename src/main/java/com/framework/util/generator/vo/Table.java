package com.framework.util.generator.vo;

import java.util.List;

/**
 * 表
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月1日 下午5:25:51
 */
public class Table {

	private String tableName;// 表名
	private List<Column> columns;// 所有列
	private String clazzName;// javabean name

	public Table() {
	}

	public Table(String tableName) {
		this.tableName = tableName;
	}

	public Table(String tableName, List<Column> columns) {
		this.tableName = tableName;
		this.columns = columns;
	}

	public Table(String tableName, List<Column> columns, String clazzName) {
		this.tableName = tableName;
		this.columns = columns;
		this.clazzName = clazzName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public String getClazzName() {
		return clazzName;
	}

	public void setClazzName(String clazzName) {
		this.clazzName = clazzName;
	}

}
