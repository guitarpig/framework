package com.framework.util.generator;

import java.util.List;

import com.framework.util.generator.vo.Table;

/**
 * 根据模板生成文件
 * @Author: lbl 543414648@qq.com
 * @Date: 2014-7-24 下午2:46:23
 */
public interface Generate {

	/**
	 * 
	 * TODO 方法作用：单表生成
	 * 
	 * @param table
	 * @throws Exception
	 * @Author: luobaolin
	 * @Date: 2014-7-24 下午4:30:26
	 */
	public void generate(Table table) throws Exception;

	/**
	 * 
	 * TODO 方法作用：多表生成
	 * 
	 * @param tables
	 * @throws Exception
	 * @Author: luobaolin
	 * @Date: 2014-7-24 下午4:30:38
	 */
	public void generate(List<Table> tables) throws Exception;

}
