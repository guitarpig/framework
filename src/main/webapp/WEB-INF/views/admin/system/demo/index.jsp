<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html>
<head>
<meta name="department" content="width=device-width" />
<title>示例演示</title>
<!--框架必需start-->
<link href="${ctx }static/css/framework.css" rel="stylesheet">
<script src="${ctx }static/js/jquery-1.8.2.min.js"></script>
<script src="${ctx }static/js/framework.js"></script>
<!--框架必需end-->
<!--jqgrid表格组件start-->
<script src="${ctx }static/js/jqgrid/jquery-ui-custom.min.js"></script>
<script src="${ctx }static/js/jqgrid/grid.local-cn.js"></script>
<link href="${ctx }static/js/jqgrid/css/jqgrid.css" rel="stylesheet" />
<script src="${ctx }static/js/jqgrid/jqGrid.js"></script>
<!--布局组件start-->
<script src="${ctx }static/js/layout/splitter.js"></script>
<!--布局组件end-->
<!--表单验证组件start-->
<script src="${ctx }static/js/validator/framework-validator.js"></script>
<!--表单验证组件end-->
</head>
<body>
	<div>
<script type="text/javascript">
    
</script>
		
</body>
</html>
