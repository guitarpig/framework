<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html>
<head>
<link href="http://121.40.148.178:8080/Content/Scripts/lhgdialog/default.css" rel="stylesheet" id="lhgdialoglink">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<title>首页-小说站后台管理系统</title>
<!--框架必需start-->
<link href="${ctx }static/css/startmenu.css" rel="stylesheet">
<link href="${ctx }static/css/accordion.css" rel="stylesheet">
<link href="${ctx }static/css/framework.css" rel="stylesheet">
<script src="${ctx }static/js/jquery-1.8.2.min.js"></script>
<script src="${ctx }static/js/framework.js"></script>
<!--框架必需end-->
<!--引入弹窗组件start-->
<script src="${ctx }static/js/lhgdialog/lhgdialog.min.js"></script>
<!--引入弹窗组件end-->
<!--自定义滚动条组件start-->
<script src="${ctx }static/js/scrollbar/scrollbar.js"></script>
<!--自定义滚动条组件end-->
<!--日期组件start-->
<script src="${ctx }static/js/datepicker/WdatePicker.js"></script>
<link href="${ctx }static/js/datepicker/skin/WdatePicker.css" rel="stylesheet">
<!--日期组件start-->
<script src="${ctx }static/js/index.js"></script>
<script>
	/**初始化**/
	$(document).ready(function() {
				serverCurrentTime()
				/* addTabMenu('Imain', '/Home/AccordionPage', '欢迎首页', "house.png",
						'false'); */
				//getAccordionMenu();
				initializeImpact();
				//shortcutsList();
				var interval = setInterval("IconSong('icon_message')", 400);
				$("#div_icon_message").click(
						function() {
							clearInterval(interval);
							window.open('http://www.learun.cn/fdms/index.html',
									'_blank');
						})
				$(".popup li").click(function() {
					linkaddTabMenu()
				})
			});
	//点击菜单连接（隐藏导航菜单）
	function linkaddTabMenu() {
		$('.btn-nav-toggle').removeAttr('disabled');
		$('.btn-nav-toggle').removeClass('harvest');
		$('.btn-nav-toggle').trigger("click");
		//点击Tab事件
		$('#tabs_container li').click(function() {
			var id = $(this).attr('id');
			if (id == 'tabs_Imain') {
				$('.btn-nav-toggle').attr('disabled', 'disabled');
				//点击首页（显示导航菜单）
				$(".navigation").css('position', '');
				$(".navigation").css('width', '204');
				$('.accordion').show();
				$('.btn-nav-toggle').addClass('harvest');
				$('.btn-nav-toggle').find('b').hide();
				$('.btn-nav-toggle').find('i').show();
				$('.btn-nav-toggle').attr('title', '');
			} else {
				$('.btn-nav-toggle').removeAttr('disabled');
				//（隐藏导航菜单）
				$(".navigation").css('position', 'absolute');
				$('.btn-nav-toggle').removeClass('harvest');
				$('.btn-nav-toggle').trigger("click");
			}
		});
	}
	//初始化界面UI效果
	function initializeImpact() {
		//设置自应高度
		resizeU();
		$(window).resize(resizeU);
		function resizeU() {
			var divkuangH = $(window).height();
			$(".mainPannel").height(divkuangH - 130);
			$(".navigation").height(divkuangH - 130);
			$("#ContentPannel").height(divkuangH - 130);
		}
		//手风琴效果
		var Accordion = function(el, multiple) {
			this.el = el || {};
			this.multiple = multiple || false;
			var links = this.el.find('.link');
			links.on('click', {
				el : this.el,
				multiple : this.multiple
			}, this.dropdown)
		}
		Accordion.prototype.dropdown = function(e) {
			//计算高度
			var accordionheight = ($("#accordion").children("ul li").length * 36);
			var navigationheight = $(".navigation").height()
			$('#accordion li').children('.b-children').height(
					navigationheight - accordionheight - 1);
			$(this).next().slideToggle();
			$(this).parent().toggleClass('open');
			if (!e.data.multiple) {
				$(this).parent().parent().find('.submenu').not($(this).next())
						.slideUp().parent().removeClass('open');
			}
			;
		}
		$(".submenu a").click(function() {
			$('.submenu a').removeClass('action');
			$(this).addClass('action');
		})
		var accordion = new Accordion($('#accordion'), false);
		$("#accordion li:first").find('div').trigger("click");//默认第一个展开
		$('.btn-nav-toggle').click(
				function() {
					if (!$('.btn-nav-toggle').attr('disabled')
							&& !$(this).hasClass("harvest")) {
						$(this).addClass('harvest');
						$(".navigation").animate({
							width : 0
						}, 100);
						$('.accordion').hide();
						$(this).find('b').show();
						$(this).find('i').hide();
					} else {
						$(this).removeClass('harvest');
						$(".navigation").animate({
							width : 204
						}, 100);
						$('.accordion').show();
						$(this).find('b').hide();
						$(this).find('i').show();
					}
				}).hover(function() {
			if ($(this).hasClass("harvest")) {
				$(this).attr('title', '隐藏导航');
				$(this).removeClass('harvest');
				$(".navigation").animate({
					width : 204
				}, 100);
				$('.accordion').show();
				$(this).find('b').hide();
				$(this).find('i').show();
				$(".navigation").css('position', 'absolute');
			}
		}, function() {
		});
	}
	/*导航菜单begin====================*/
	//导航一级菜单
	var accordionJson = "";
	function getAccordionMenu() {
		var html = "";
		getAjax(
				"/Home/LoadAccordionMenu",
				"",
				function(data) {
					accordionJson = eval("(" + data + ")");
					$
							.each(
									accordionJson,
									function(i) {
										if (accordionJson[i].ParentId == '9f8ce93a-fc2d-4914-a59c-a6b49494108f') {
											html += "<li title=" + accordionJson[i].FullName + ">";
											html += "<div class=\"link\"><img src='${ctx}static/images/16/" + accordionJson[i].Icon + "'>";
											html += "<span>"
													+ accordionJson[i].FullName
													+ "</span><i class=\"chevron-down\"></i>";
											html += "</div>";
											html += GetSubmenu(
													accordionJson[i].ModuleId,
													"b-children");
											html += "</li>";
										}
									});
				})
		$("#accordion").append(html);
	}
	//导航子菜单
	function GetSubmenu(ModuleId, _class) {
		var submenu = "<ul class=\"submenu " + _class + "\">";
		$
				.each(
						accordionJson,
						function(i) {
							if (accordionJson[i].ParentId == ModuleId) {
								if (IsBelowMenu(accordionJson[i].ModuleId) > 0) {
									submenu += "<li title=" + accordionJson[i].FullName + "><a class=\"link\"><img src='${ctx}static/images/16/" + accordionJson[i].Icon + "'><span>"
											+ accordionJson[i].FullName
											+ "</span><i class=\"submenu-chevron-down\"></i></a>";
									submenu += GetSubmenu(
											accordionJson[i].ModuleId,
											"c-children")
									submenu += "</li>";
								} else {
									submenu += "<li title="
											+ accordionJson[i].FullName
											+ " onclick=\"addTabMenu('"
											+ accordionJson[i].ModuleId
											+ "', '"
											+ RootPath()
											+ accordionJson[i].Location
											+ "', '"
											+ accordionJson[i].FullName
											+ "',  '"
											+ accordionJson[i].Icon
											+ "','true');linkaddTabMenu()\"><img src='${ctx}static/images/16/" + accordionJson[i].Icon + "'><a><span>"
											+ accordionJson[i].FullName
											+ "</span></a></li>";
								}
							}
						});
		submenu += "</ul>";
		return submenu;
	}
	//判断是否有子节点
	function IsBelowMenu(ModuleId) {
		var count = 0;
		$.each(accordionJson, function(i) {
			if (accordionJson[i].ParentId == ModuleId) {
				count++;
				return false;
			}
		});
		return count;
	}
</script>
</head>
<body onbeforeunload="PageClose()" onselectstart="return false;"
	style="-moz-user-select: none; overflow: hidden;">
	<div class=""
		style="left: 0px; top: 0px; visibility: hidden; position: absolute;">
		<table class="ui_border">
			<tbody>
				<tr>
					<td class="ui_lt"></td>
					<td class="ui_t"></td>
					<td class="ui_rt"></td>
				</tr>
				<tr>
					<td class="ui_l"></td>
					<td class="ui_c"><div class="ui_inner">
							<table class="ui_dialog">
								<tbody>
									<tr>
										<td colspan="2"><div class="ui_title_bar">
												<div class="ui_title" unselectable="on"
													style="cursor: move;"></div>
												<div class="ui_title_buttons">
													<a class="ui_min" href="javascript:void(0);" title="最小化"
														style="display: inline-block;"><b class="ui_min_b"></b></a><a
														class="ui_max" href="javascript:void(0);" title="最大化"
														style="display: inline-block;"><b class="ui_max_b"></b></a><a
														class="ui_res" href="javascript:void(0);" title="还原"><b
														class="ui_res_b"></b><b class="ui_res_t"></b></a><a
														class="ui_close" href="javascript:void(0);"
														title="关闭(esc键)" style="display: inline-block;">×</a>
												</div>
											</div></td>
									</tr>
									<tr>
										<td class="ui_icon" style="display: none;"></td>
										<td class="ui_main" style="width: auto; height: auto;"><div
												class="ui_content" style="padding: 0px;"></div></td>
									</tr>
									<tr>
										<td colspan="2"><div class="ui_buttons"
												style="display: none;"></div></td>
									</tr>
								</tbody>
							</table>
						</div></td>
					<td class="ui_r"></td>
				</tr>
				<tr>
					<td class="ui_lb"></td>
					<td class="ui_b"></td>
					<td class="ui_rb" style="cursor: se-resize;"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="ajax-loader"
		style="cursor: progress; position: fixed; top: -50%; left: -50%; width: 200%; height: 200%; z-index: 100; overflow: hidden; display: none; background: rgb(255, 255, 255);">
		<img src="/Content/Images/ajax-loader.gif"
			style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; margin: auto;"> 
	</div>
	<!-- header -->
	<div class="header">
		<div class="logo fleft">
			<!-- logo 
			<img src="/Content/Images/loginlogo.png">
			-->
		</div>
		<div id="Headermenu">
			<ul id="topnav">
				<li id="metnav_1" class="list"><a id="nav_1"
					onclick="Replace();"> <span class="c1"></span> 系统首页
				</a></li>
				<li id="metnav_7" class="list droppopup"><a id="nav_7"> <span
						class="c7"></span>快捷导航
						<div class="popup">
							<i></i>
							<ul>
								<li onclick="Shortcuts()"><img
									src="${ctx }static/images/16/shortcuts.png">快捷方式设置</li>
								<div id="Shortcuts"></div>
							</ul>
						</div>
				</a></li>
				<li id="metnav_3" class="list droppopup"><a id="nav_3" class="">
						<span class="c3"></span>帮助中心
						<div class="popup" style="display: none; top: 74px; left: 1095px;">
							<i></i>
							<ul>
								<li><img src="/Content/Images/16/help.png">查看帮助</li>
								<li title="将反馈建议提交给开发商进行解决"
									onclick=" window.open('http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&amp;email=uICJioyLiYuPivjJyZbb19U','_blank')">
									<img src="/Content/Images/16/email_open.png">反馈建议
								</li>
								<li onclick="Support()"><img
									src="/Content/Images/16/premium_support.png">技术支持</li>
								<li onclick="About()"><img
									src="/Content/Images/16/information.png">关于我们</li>
							</ul>
						</div>
				</a></li>
				<li id="metnav_2" class="list" onclick="SkinIndex()"><a
					id="nav_2"> <span class="c2"></span>切换皮肤
				</a></li>
				<li id="metnav_5" class="list" onclick="PersonCenter()"><a
					id="nav_5"> <span class="c5"></span>个人中心
				</a></li>
				<li id="metnav_4" class="list" onclick="IndexOut();"><a
					id="nav_4"> <span class="c4"></span> 安全退出
				</a></li>
			</ul>
		</div>
	</div>
	<div class="taskbarTabs">
		<div id="navigationtitle">
			<div id="CurrentTime" style="float: left; padding-left: 12px;">2015年05月15日
				14:07:16</div>
			<div disabled="" style="float: right;" class="btn-nav-toggle">
				<i></i> <b></b>
			</div>
		</div>
		<div style="float: left">
			<div id="dww-menu" class="mod-tab">
				<div class="mod-hd">
					<ul id="tabs_container" class="tab-nav">
						<li id="tabs_Imain" class="selected"
							onclick="addTabMenu('Imain','${ctx}admin','欢迎首页','false')"><a><img
								src="${ctx}static/images/16/house.png" width="16" height="16">欢迎首页</a></li>
					</ul>
				</div>
				<input id="ModuleId" type="hidden" value="Imain">
			</div>
			<div class="rightMenu" style="display: none;">
				<ul>
					<li onclick="top.frames[tabiframeId()].replace()">刷新当前</li>
					<li onclick="thisCloseTab()">关闭当前</li>
					<li onclick="allcloseTab()">全部关闭</li>
					<li onclick="othercloseTab()">除此之外全部关闭</li>
					<div class="m-split"></div>
					<li>退出</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="mainPannel" style="height: 232px;">
		<div class="navigation" style="height: 232px;">
			<ul id="accordion" class="accordion">
				<li title="系统管理" class="">
					<div class="link">
						<img src="${ctx }static/images/16/widgets.png"> <span>系统管理</span><i
							class="chevron-down"> </i>
					</div>
					<ul class="submenu b-children"
						style="height: 399px; display: none;">
						<li title="部门管理"
							onclick="addTabMenu('c108ef45-b8b6-493e-951a-9050706e2bba', '${ctx}admin/system/department', '部门管理',  'cog.png','true');linkaddTabMenu()">
							<img src="${ctx}static/images/16/cog.png"><a><span>部门管理</span></a>
						</li>
						<li title="案例演示"
							onclick="addTabMenu('c108ef45-b8b6-493e-951a-9050706e2bba', '${ctx}admin/demo/convertDemo', '案例演示',  'cog.png','true');linkaddTabMenu()">
							<img src="${ctx}static/images/16/cog.png"><a><span>案例演示</span></a>
						</li>
						<li title="数据字典"
							onclick="addTabMenu('8104bfa2-fc85-4459-b533-7f58b7541155', 'http://121.40.148.178:8080/CommonModule/DataDictionary/Index', '数据字典',  'books.png','true');linkaddTabMenu()">
							<img src="${ctx}static/images/16/books.png"><a><span>数据字典</span></a>
						</li>
						<li title="智能开发" class=""><a class="link"><img
								src="${ctx}static/images/16/monitor_window_3d.png"><span>智能开发</span><i
								class="submenu-chevron-down"></i></a>
							<ul class="submenu c-children" style="display: none;">
								<li title="系统模块"
									onclick="addTabMenu('5c5077f0-7703-4fee-927a-b765e1edf900', 'http://121.40.148.178:8080/CommonModule/Module/Index', '系统模块',  'navigation.png','true');linkaddTabMenu()">
									<img src="${ctx}static/images/16/navigation.png"><a><span>系统模块</span></a>
								</li>
								<li title="系统按钮"
									onclick="addTabMenu('c3a88954-7a75-46bb-a155-2ac1b41e87d9', 'http://121.40.148.178:8080/CommonModule/Button/Index', '系统按钮',  'bricks.png','true');linkaddTabMenu()">
									<img src="${ctx}static/images/16/bricks.png"><a><span>系统按钮</span></a>
								</li>
								<li title="系统视图"
									onclick="addTabMenu('5477b88b-3393-4d39-ba2d-f219f486bd38', 'http://121.40.148.178:8080/CommonModule/View/Index', '系统视图',  'application_view_list.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/application_view_list.png"><a><span>系统视图</span></a></li>
								<li title="系统表单"
									onclick="addTabMenu('2b057961-5ed1-4785-b808-1f366085f406', 'http://121.40.148.178:8080/CommonModule/FormLayout/Index', '系统表单',  'application_form.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/application_form.png"><a><span>系统表单</span></a></li>
								<li title="异常文件"
									onclick="addTabMenu('62884e64-63a4-4790-9e4c-24003e0e278c', 'http://121.40.148.178:8080/CommonModule/SysLog/FileIndex', '异常文件',  'file_extension_txt.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/file_extension_txt.png"><a><span>异常文件</span></a></li>
								<li title="代码生成"
									onclick="addTabMenu('6d2b6f5b-64bc-42c1-8fa6-3c3da9e019c1', 'http://121.40.148.178:8080/CodeMaticModule/CodeMatic/CodeMaticIndex', '代码生成',  'page_code.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/page_code.png"><a><span>代码生成</span></a></li>
								<li title="接口API"
									onclick="addTabMenu('aad50a42-9077-42ff-9b39-ba2e640bdd8a', 'http://121.40.148.178:8080/CommonModule/InterfaceManage/Index', '接口API',  'disconnect.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/disconnect.png"><a><span>接口API</span></a></li>
								<li title="系统视图"
									onclick="addTabMenu('5477b88b-3393-4d39-ba2d-f219f486bd38', 'http://121.40.148.178:8080/CommonModule/View/Index', '系统视图',  'application_view_list.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/application_view_list.png"><a><span>系统视图</span></a></li>
								<li title="系统表单"
									onclick="addTabMenu('2b057961-5ed1-4785-b808-1f366085f406', 'http://121.40.148.178:8080/CommonModule/FormLayout/Index', '系统表单',  'application_form.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/application_form.png"><a><span>系统表单</span></a></li>
								<li title="异常文件"
									onclick="addTabMenu('62884e64-63a4-4790-9e4c-24003e0e278c', 'http://121.40.148.178:8080/CommonModule/SysLog/FileIndex', '异常文件',  'file_extension_txt.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/file_extension_txt.png"><a><span>异常文件</span></a></li>
								<li title="代码生成"
									onclick="addTabMenu('6d2b6f5b-64bc-42c1-8fa6-3c3da9e019c1', 'http://121.40.148.178:8080/CodeMaticModule/CodeMatic/CodeMaticIndex', '代码生成',  'page_code.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/page_code.png"><a><span>代码生成</span></a></li>
								<li title="接口API"
									onclick="addTabMenu('aad50a42-9077-42ff-9b39-ba2e640bdd8a', 'http://121.40.148.178:8080/CommonModule/InterfaceManage/Index', '接口API',  'disconnect.png','true');linkaddTabMenu()"><img
									src="${ctx}static/images/16/disconnect.png"><a><span>接口API</span></a></li>
							</ul></li>
						<li title="数据库管理"
							onclick="addTabMenu('c0969b4f-8925-43f3-b664-f1114a4e9aed', 'http://121.40.148.178:8080/CommonModule/DataBase/Index', '数据库管理',  'server_database.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/server_database.png"><a><span>数据库管理</span></a></li>
						<li title="系统日志"
							onclick="addTabMenu('1af02b2d-bb66-4716-9ed5-ec3fcff9f5e2', 'http://121.40.148.178:8080/CommonModule/SysLog/Index', '系统日志',  'file_extension_log.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/file_extension_log.png"><a><span>系统日志</span></a></li>
						<li title="单据编码"
							onclick="addTabMenu('a230b2f5-b9bb-498b-b03a-3540d33110d0', 'http://121.40.148.178:8080/CommonModule/CodeRule/Index', '单据编码',  'text_list_numbers.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/text_list_numbers.png"><a><span>单据编码</span></a></li>
						<li title="导入配置"
							onclick="addTabMenu('9840dd23-400a-4cf9-a27a-2ab990c3537f', 'http://121.40.148.178:8080/CommonModule/ExcelImport/Index', '导入配置',  'sharepoint.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/sharepoint.png"><a><span>导入配置</span></a></li>
					</ul>
				</li>
				<li title="基础资料" class=""><div class="link">
						<img src="${ctx}static/images/16/setting_tools.png"><span>基础资料</span><i
							class="chevron-down"></i>
					</div>
					<ul class="submenu b-children"
						style="height: 399px; display: none;">
						<li title="公司管理"
							onclick="addTabMenu('b29cabd8-ffb6-4d34-9d08-ee1dba2b5b6b', 'http://121.40.148.178:8080/CommonModule/Company/Index', '公司管理',  'house.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/house.png"><a><span>公司管理</span></a></li>
						<li title="部门管理"
							onclick="addTabMenu('e84c0fca-d912-4f5c-a25e-d5765e33b0d2', 'http://121.40.148.178:8080/CommonModule/Department/Index', '部门管理',  'folder_user.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/folder_user.png"><a><span>部门管理</span></a></li>
						<li title="角色管理"
							onclick="addTabMenu('cef74b80-24a5-4d77-9ede-bbbc75cdb431', 'http://121.40.148.178:8080/CommonModule/Roles/Index', '角色管理',  'role.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/role.png"><a><span>角色管理</span></a></li>
						<li title="岗位管理"
							onclick="addTabMenu('eb0c4d65-4757-4892-b2e9-35882704e592', 'http://121.40.148.178:8080/CommonModule/Post/Index', '岗位管理',  'outlook_new_meeting.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/outlook_new_meeting.png"><a><span>岗位管理</span></a></li>
						<li title="用户组管理"
							onclick="addTabMenu('b863d076-37bb-45aa-8318-37942026921e', 'http://121.40.148.178:8080/CommonModule/GroupUser/Index', '用户组管理',  'group_gear.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/group_gear.png"><a><span>用户组管理</span></a></li>
						<li title="用户管理"
							onclick="addTabMenu('58e86c4c-8022-4d30-95d5-b3d0eedcc878', 'http://121.40.148.178:8080/CommonModule/User/Index', '用户管理',  'report_user.png','true');linkaddTabMenu()"><img
							src="${ctx}static/images/16/report_user.png"><a><span>用户管理</span></a></li>
					</ul></li>
			</ul>
		</div>
		<div id="overlay_navigation"></div>
		<div id="ContentPannel" style="height: 232px;">
			<iframe id="tabs_iframe_Imain" name="tabs_iframe_Imain" height="100%"
				width="100%" src="/Home/AccordionPage" frameborder="0"></iframe>
		</div>
	</div>
	<div id="footer" class="cs-south" style="height: 25px;">
		<div class="number"
			style="width: 30%; text-align: left; float: left; line-height: 25px;">
			&nbsp;技术支持：<a href="http://www.learun.cn/fdms/index.html"
				target="_blank" style="color: white;">宜昌金蜘蛛计算机信息技术有限公司</a>
		</div>
		<div class="number"
			style="width: 40%; text-align: center; float: left; line-height: 25px;">
			CopyRight © 2010 - 2015 By jzz</div>
		<div style="width: 30%; text-align: right; float: right;">
			<div style="padding-right: 0px;">
				<div title="在线用户（52）人" class="bottom_icon" style="padding-top: 2px;">
					<img src="/Content/Images/bottom_icon_usergroup.png">
				</div>
				<div title="邮件消息" class="bottom_icon" style="padding-top: 2px;">
					<img id="icon_email" src="/Content/Images/youjian.png"
						style="padding-top: 5px;">
				</div>
				<div id="div_icon_message" title="即时消息" class="bottom_icon"
					style="padding-top: 2px;">
					<img id="icon_message"
						src="/Content/Images/bottom_icon_message.png" class=""
						style="display: inline;">
				</div>
				<div class="bottom_icon" style="padding-top: 1px;">
					<img title="我的信息，账户：System（超级管理员）"
						src="/Content/Images/bottom_icon_userinfo.png">
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!--载进度条start-->
	<div id="loading_background" class="loading_background"
		style="display: none;"></div>
	<div id="loading" onclick="Loading(false);"
		style="left: 620.5px; display: none;">
		<img src="/Content/Images/loading.gif" style="vertical-align: middle;">&nbsp;<span>正在拼了命为您加载…</span>&nbsp;
	</div>
	<div id="loadingGird">
		<img src="/Content/Images/loading.gif" style="vertical-align: middle;">&nbsp;正在拼了命为您加载…&nbsp;
	</div>
</body>
</html>