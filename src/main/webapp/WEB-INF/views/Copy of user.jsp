<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>小说站后台管理系统-用户帐户</title>
<link href="${ctx }static/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="${ctx }static/js/jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="${ctx }static/js/jqwidgets/styles/jqx.arctic.css" type="text/css" />
<script type="text/javascript" src="${ctx }static/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxdata.js"></script> 
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxcombobox.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.storage.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.columnsreorder.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxgrid.selection.js"></script> 
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="${ctx }static/js/jqwidgets/globalization/globalize.js"></script>
<script src="${ctx }static/js/common.js" type="text/javascript"></script>
<script src="${ctx }static/js/FunctionJS.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function () {
            var url = "${ctx}admin/user/list";
            // prepare the data
           	var source =
            {
           		type: 'post',
                datatype: "json",
                datafields: [
                    { name: 'userName', type: 'string' },
                    { name: 'realName', type: 'string' },
                    { name: 'gender', type: 'int' },
                    { name: 'mobile', type: 'string' },
                    { name: 'email', type: 'string' },
                    { name: 'enabled', type: 'int' },
                    { name: 'logOnCount', type: 'int' },
                    { name: 'lastVisit', type: 'date' },
                    { name: 'description', type: 'string' }
                ],
                filter: function(filters, recordsArray){
                	//alert(JSON.stringify(filters));
                	$('#jqxgrid').jqxGrid('updatebounddata');
                },
                id: 'uid',
                url: url,
                root: 'data'
            };
            
            var rendergridrows = function (params) {
            	var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
                return params.data;
            }
            
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
            	width : '99.8%',
				height : $(window).height() - 52,
                source: dataAdapter,
                theme: 'arctic',
                altrows: true,//开启交替行背景色
                sortable: true,//开启排序
                filterable: true,//开启过滤器
                pageable: true,
                pagesize: 20,
                virtualmode: true,//虚拟分页 
                showfiltermenuitems: false,//不显示过滤器选项 
                showsortmenuitems: false,//不显示排序选项
                showfilterrow: true, //开启行过滤器
                selectionmode: 'multiplerowsextended',//选中多行
                rendergridrows: rendergridrows,
                columnsresize: true,
                /*showtoolbar: true,
                toolbarheight: 34,
                rendertoolbar: function (toolbar) {
                	var me = this;
                    var container = $("<div style='margin: 5px;' ></div>");
                    toolbar.append(container);
                    container.append("<img src='${ctx}static/images/32/202323.png' width='25' height='25' style=\"vertical-align: middle;\" />用户列表");
                },*/
                columns: [
                     {
                         text: '', sortable: false, filterable: false, editable: false,
                         groupable: false, draggable: false, resizable: false,
                         datafield: '', columntype: 'number', width: '4%',
                         cellsrenderer: function (row, column, value) {
                             return "<div style='width:50px;text-align:center;margin-top:6px'>" + (value + 1) + "</div>";
                         }
                     },
                     { text: '登陆账号', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'userName', filterdelay: 1000, width: '10%',
                    	 createfilterwidget: function (column, htmlElement, editor) {
                    		
                         }
                     },
                     { text: '姓名', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'realName', filterdelay: 1000, width: '10%' },
                     { text: '性别', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'gender', filtertype: 'list', filterdelay: 1000, width: '4%', 
                    	 cellsrenderer: function (row, column, value, defaulthtml, columnproperties) {
                    		var newhtml = $(defaulthtml);
                    		return $(defaulthtml).html(value?"男":"女")[0].outerHTML;
                         },
                         createfilterwidget: function (column, htmlElement, editor) {
                        	 var genderData = [{label : "男", value : "1"},{label : "女", value : "0"}];
                        	 
                        	 var genderSource =
                             {
                        		 localdata: genderData,
                                 datatype: "array",
                                 datafields: [
                                     { name: 'label' },
                                     { name: 'value' }
                                 ]
                             };
                             var genderDataAdapter = new $.jqx.dataAdapter(genderSource);
                             editor.jqxDropDownList({source:genderData, selectedIndex: 0, autoDropDownHeight: true, displayMember: "label", valueMember: "value", width: 40, dropDownWidth: 40});

                         }
                     },
                     { text: '手机号码', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'mobile', filterdelay: 1000, width: '10%' },
                     { text: '电子邮箱', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'email', filterdelay: 1000, width: '12%' },
                     { text: '有效性', datafield: 'enabled', cellsalign: 'center', align: 'center', threestatecheckbox: true, columntype: 'checkbox', filtertype: 'bool', filterdelay: 1000, width: '4%' },
                     { text: '登陆次数', filterable:false, cellsalign: 'center', align: 'center', datafield: 'logOnCount', width: '6%' },
                     { text: '最后登陆时间', cellsalign: 'center', align: 'center', datafield: 'lastVisit', cellsformat: 'yyyy-MM-dd HH-mm-ss', filtertype: 'range', filterdelay: 1000, width: '20%', 
                    	 createfilterwidget: function (column, htmlElement, editor) {
                    		 //设置过滤器部件
                    	     editor.jqxDateTimeInput({culture: 'zh-CN', todayString: '今天', clearString: '清除' });
                         }	 
                     },
                     { text: '说明', filterable:false, cellsalign: 'center', align: 'center', datafield: 'description', filterdelay: 1000, width: '20%' }
                ],
   	            columngroups: [
   	              { text: '基本信息', align: 'center', name: 'baseInfo' }
   	            ]
            });
        });
        
        function add(){
        	var filtergroup = new $.jqx.filter();
            var filter_or_operator = 1;
            var filtervalue = 'User';
            var filtercondition = 'starts_with';
            var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
            filtervalue = 'Andrew';
            filtercondition = 'starts_with';
            var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

            filtergroup.addfilter(filter_or_operator, filter1);
            //filtergroup.addfilter(filter_or_operator, filter2);
            // add the filters.
            $("#jqxgrid").jqxGrid('addfilter', 'realName', filtergroup);
            // apply the filters.
            $("#jqxgrid").jqxGrid('applyfilters');
        }
    </script>
</head>
<body>
	<form method="post" action="UserList.aspx" id="form1">
		<div class="aspNetHidden">
			<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE"
				value="/wEPDwUKMTA0NTg4NDE5NWQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFB0VuYWJsZWS60Ymy0dDayjrLWMsnxpytXFuD8qcPtrsXVZZXyRGMBA==" />
		</div>

		<div class="aspNetHidden">

			<input type="hidden" name="__VIEWSTATEGENERATOR"
				id="__VIEWSTATEGENERATOR" value="7D172D7D" /> <input type="hidden"
				name="__EVENTVALIDATION" id="__EVENTVALIDATION"
				value="/wEdAARoucJgeoGypXLHkCBSb5S/0/KdySeuRpi7LRO6xG0ppnP+nVcLtgM89QbAfF8oONKHANx17rhBI97RQU9UWB7TsufxVhF42omCcCfLHBSkhQg4iJL0m1qmmFXuQSGq5qM=" />
		</div>

		<div class="tools_bar">
			<a title="刷新当前页面" onclick="Replace();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/arrow_refresh.png') 50% 4px no-repeat;">刷新</b></span></a>
			<div class="tools_separator"></div>
			<a title="" onclick="add();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/application_add.png') 50% 4px no-repeat;">新增</b></span></a><a
				title="" onclick="edit();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/application_edit.png') 50% 4px no-repeat;">编辑</b></span></a><a
				title="" onclick="Delete();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/application_delete.png') 50% 4px no-repeat;">删除</b></span></a>
			<div class="tools_separator"></div>
			<a title="查看相关详细信息" onclick="lookup();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/page_white_find.png') 50% 4px no-repeat;">查看详细</b></span></a><a
				title="重新设置新密码" onclick="SetNewPassword();;" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/group_key.png') 50% 4px no-repeat;">重置密码</b></span></a><a
				title="用户分配角色" onclick="AllotRole();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/allot_role.png') 50% 4px no-repeat;">分配角色</b></span></a><a
				title="Excel导入" onclick="import();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/enter.png') 50% 4px no-repeat;">引入</b></span></a><a
				title="导出" onclick="derive();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/out.png') 50% 4px no-repeat;">引出</b></span></a>
			<div class="tools_separator"></div>
			<a title="关闭当前窗口" onclick="ThisCloseTab();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/back.png') 50% 4px no-repeat;">离开</b></span></a>
		</div>
		<!-- 
		<div class="btnbarcontetn" style="margin-top: 1px; background: #fff">
			<div>
				<table border="0" class="frm-find" style="height: 45px;">
					<tr>
						<th>查询条件：</th>
						<td><select name="query" id="query" class="select"
							style="width: 70px">
								<option value="Code">编号</option>
								<option value="Account">账户</option>
								<option value="RealName">姓名</option>
								<option value="Mobile">手机号码</option>
								<option value="DepartmentId">部门</option>
						</select></td>
						<th>关键字：</th>
						<td><input name="keywords" type="text" id="keywords"
							class="txt" style="width: 200px" /></td>
						<td><input id="btnSearch" type="button" class="btnSearch"
							value="搜 索" onclick="ListGrid()" /> <span class="item"> <input
								name="Enabled" type="checkbox" id="Enabled" onclick="ListGrid()"
								checked="checked" style="vertical-align: middle;" /> <label
								for="Enabled" style="vertical-align: middle;">只显示有效的</label>
								&nbsp;&nbsp;
						</span></td>
					</tr>
				</table>
			</div>
		</div> --> 
		<div id="jqxgrid" style="margin-top: 1px;"></div>
	</form>
</body>