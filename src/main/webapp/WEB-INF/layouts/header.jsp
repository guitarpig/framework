<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}/"/>
<div class="topheader">
    <div class="left">
        <h1 class="logo">小说站<span></span></h1>
        <span class="slogan">后台管理系统</span>
        <br clear="all" />
    </div><!--left-->
    
    <div class="right">
    	<!--<div class="notification">
            <a class="count" href="ajax/notifications.html"><span>9</span></a>
    	</div>-->
        <div class="userinfo">
        	<img src="${ctx }static/images/thumbs/avatar.png" alt="" />
            <span>管理员</span>
        </div><!--userinfo-->
        
        <div class="userinfodrop">
        	<div class="avatar">
            	<a href=""><img src="${ctx }static/images/thumbs/avatarbig.png" alt="" /></a>
                <div class="changetheme">
                	切换主题: <br />
                	<a class="default"></a>
                    <a class="blueline"></a>
                    <a class="greenline"></a>
                    <a class="contrast"></a>
                    <!-- <a class="custombg"></a> -->
                </div>
            </div><!--avatar-->
            <div class="userdata">
            	<h4>Juan</h4>
                <span class="email">youremail@yourdomain.com</span>
                <ul>
                	<li><a href="editprofile.html">编辑资料</a></li>
                    <li><a href="accountsettings.html">账号设置</a></li>
                    <li><a href="help.html">帮助</a></li>
                    <li><a href="index.html">退出</a></li>
                </ul>
            </div><!--userdata-->
        </div><!--userinfodrop-->
    </div><!--right-->
</div><!--topheader-->
    
<div class="header">
	<ul class="headermenu">
    	<li class="current"><a href="dashboard.html"><span class="icon icon-flatscreen"></span>首页</a></li>
        <li><a href="messages.html"><span class="icon icon-message"></span>查看消息</a></li>
        <li><a href="reports.html"><span class="icon icon-chart"></span>统计报表</a></li>
    </ul>
</div><!--header-->