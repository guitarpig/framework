/**
 * 设置日历语言
 */
//$.getScript('../jqwidgets/globalization/globalize.culture.zh-CN.js', function () {
//                                $("#jqxDateTimeInput").jqxDateTimeInput({ culture: 'zh-CN' });
//                            });
/**
 * 设置全局主题
 */
$(document).ready(function () {
	if($.jqx) {
		$.jqx.theme = "arctic";
	}
})


/**
 * 封装ajax提交
 * @param url 提交后台方法路径
 * @param data 提交参数
 * @param func 回调方法 
 */
function ajaxSubmit(url, data, func) {
	$.ajax({
		url : url,
		type : "post",
		async : true,
		dataType : "json",
		data : data,
		timeout : 60000,
		success : function(reData) {
			if ($.trim(reData) == null) {
				window.location.href = "/admin"; // 首页地址
			} else {
				func(reData);
			}
		},
		error : function(data) {
			alert(JSON.stringify(data));
			if ((data + "").indexOf("XMLHttpRequest") > 0) {
				top.showTopMsg("请求超时！", 4000, 'error');
			} else {
				top.showTopMsg("请求出错！", 4000, 'error');
			}
		}
	});

}

/* 全局常量 */
/**
 * jqwidgets过滤器
 */
var JqxFilter = {
	stringfilter: "stringfilter",
	numericfilter: "numericfilter",
	datefilter: "datefilter",
	booleanfilter: "booleanfilter",
	stringConditions:function(type){
        return [
   		    {label: "为空", value: "EMPTY"},
		    {label: "不为空", value: "NOT_MPTY"},
		    {label: "包含", value: "CONTAINS"},
		    {label: "包含(大小写敏感)", value: "CONTAINS_CASE_SENSITIVE"},
		    {label: "不包含", value: "DOES_NOT_CONTAIN"},
		    {label: "不包含(大小写敏感)", value: "DOES_NOT_CONTAIN_CASE_SENSITIVE"},
		    {label: "开始于", value: "STARTS_WITH"},
		    {label: "开始于(大小写敏感)", value: "STARTS_WITH_CASE_SENSITIVE"},
		    {label: "结束于", value: "ENDS_WITH"},
		    {label: "结束于(大小写敏感)", value: "ENDS_WITH_CASE_SENSITIVE"},
		    {label: "等于", value: "EQUAL"},
		    {label: "等于(大小写敏感)", value: "EQUAL_CASE_SENSITIVE"}
		];
	},
	numericConditions:function(){
		return [
		    {label: "等于", value: "EQUAL"},
		    {label: "不等于", value: "NOT_EQUAL"},
		    {label: "小于", value: "LESS_THAN"},
		    {label: "小于等于", value: "LESS_THAN_OR_EQUAL"},
		    {label: "大于", value: "GREATER_THAN"},
		    {label: "大于等于", value: "GREATER_THAN_OR_EQUAL"}
		];
	},
	dateConditions:function(){
		return [
		    {label: "等于", value: "EQUAL"},
		    {label: "不等于", value: "NOT_EQUAL"},
		    {label: "小于", value: "LESS_THAN"},
		    {label: "小于等于", value: "LESS_THAN_OR_EQUAL"},
		    {label: "大于", value: "GREATER_THAN"},
		    {label: "大于等于", value: "GREATER_THAN_OR_EQUAL"}
		];
	},
	booleanConditions:function(){
		return [
		    {label: "等于", value: "EQUAL"},
		    {label: "不等于", value: "NOT_EQUAL"}
		];
	},
	EMPTY: "EMPTY",
	NOT_EMPTY: "NOT_EMPTY",
	CONTAINS: "CONTAINS",
	CONTAINS_CASE_SENSITIVE: "CONTAINS_CASE_SENSITIVE",
	DOES_NOT_CONTAIN: "DOES_NOT_CONTAIN",
	DOES_NOT_CONTAIN_CASE_SENSITIVE: "DOES_NOT_CONTAIN_CASE_SENSITIVE",
	STARTS_WITH: "STARTS_WITH",
	STARTS_WITH_CASE_SENSITIVE: "STARTS_WITH_CASE_SENSITIVE",
	ENDS_WITH: "ENDS_WITH",
	ENDS_WITH_CASE_SENSITIVE: "ENDS_WITH_CASE_SENSITIVE",
	EQUAL: "EQUAL",
	NOT_EQUAL: "NOT_EQUAL",
	EQUAL_CASE_SENSITIVE: "EQUAL_CASE_SENSITIVE",
	LESS_THAN: "LESS_THAN",
	LESS_THAN_OR_EQUAL: "LESS_THAN_OR_EQUAL",
	GREATER_THAN: "GREATER_THAN",
	GREATER_THAN_OR_EQUAL: "GREATER_THAN_OR_EQUAL"
}

/**
 * 添加filter--用于jqwidgets多条件查询
 * @param filtervalue 
 * @param filtercondition
 * 1.possible conditions for string filter: 'EMPTY', 'NOT_EMPTY', 'CONTAINS', 'CONTAINS_CASE_SENSITIVE',
 * 'DOES_NOT_CONTAIN', 'DOES_NOT_CONTAIN_CASE_SENSITIVE', 'STARTS_WITH', 'STARTS_WITH_CASE_SENSITIVE',
 * 'ENDS_WITH', 'ENDS_WITH_CASE_SENSITIVE', 'EQUAL', 'EQUAL_CASE_SENSITIVE', 'NULL', 'NOT_NULL'
 * 2.possible conditions for numeric filter: 'EQUAL', 'NOT_EQUAL', 'LESS_THAN', 'LESS_THAN_OR_EQUAL', 'GREATER_THAN', 'GREATER_THAN_OR_EQUAL', 'NULL', 'NOT_NULL'
 * 3.possible conditions for date filter: 'EQUAL', 'NOT_EQUAL', 'LESS_THAN', 'LESS_THAN_OR_EQUAL', 'GREATER_THAN', 'GREATER_THAN_OR_EQUAL', 'NULL', 'NOT_NULL'
 * 4.possible conditions for boolean filter: 'EQUAL', 'NOT_EQUAL'
 * @param filtertype 'stringfilter', 'numericfilter', 'datefilter', 'booleanfilter'
 * @param filterdatafield
 */
function addFilter(id, filtervalue, filtercondition, filtertype, filterdatafield){
	var filtergroup = new $.jqx.filter();
    var filter_or_operator = 1;
    var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
    filtergroup.addfilter(filter_or_operator, filter);
    $("#" + id).jqxGrid('addfilter', filterdatafield, filtergroup);
    //$("#jqxgrid").jqxGrid('applyfilters');
}


