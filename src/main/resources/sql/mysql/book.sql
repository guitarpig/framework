/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : book

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2015-06-26 17:37:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `jzz_button`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_button`;
CREATE TABLE `jzz_button` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `deleteMark` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_button
-- ----------------------------

-- ----------------------------
-- Table structure for `jzz_menu`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_menu`;
CREATE TABLE `jzz_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `deleteMark` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ojb3urn99byrt06b9mrd6kdum` (`code`),
  UNIQUE KEY `UK_sfae30f2bmeb4n8g4pg4jh82e` (`name`),
  KEY `FK_3ntfwo1j9g3ecq3ul6036reph` (`parentId`),
  CONSTRAINT `FK_3ntfwo1j9g3ecq3ul6036reph` FOREIGN KEY (`parentId`) REFERENCES `jzz_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `jzz_organization`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_organization`;
CREATE TABLE `jzz_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `code` varchar(64) NOT NULL,
  `deleteMark` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fullName` varchar(64) NOT NULL,
  `shortName` varchar(64) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9yf6s5lx7d32h2sb2gtptio9` (`parentId`),
  CONSTRAINT `FK_9yf6s5lx7d32h2sb2gtptio9` FOREIGN KEY (`parentId`) REFERENCES `jzz_organization` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_organization
-- ----------------------------
INSERT INTO `jzz_organization` VALUES ('1', '宜昌市', '1', '10000', '', null, '', '金蜘蛛集团', null, 'http://www.hmjxt.com', null, null);
INSERT INTO `jzz_organization` VALUES ('5', '上海市', '2', '11000', '', '', '', '上海分公司', '', '', '', '1');
INSERT INTO `jzz_organization` VALUES ('6', '', '3', '11000.01', '', '', '', '部门1', '', '', '', '5');
INSERT INTO `jzz_organization` VALUES ('7', null, '3', '11000.02', '', null, '', '部门2', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('8', null, '3', '11000.03', '', null, '', '部门3', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('9', null, '3', '11000.04', '', null, '', '部门4', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('10', null, '3', '11000.05', '', null, '', '部门5', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('11', null, '3', '11000.06', '', null, '', '部门6', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('12', null, '3', '11000.07', '', null, '', '部门7', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('13', null, '3', '11000.08', '', null, '', '部门8', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('14', null, '3', '11000.09', '', null, '', '部门9', null, null, null, '5');
INSERT INTO `jzz_organization` VALUES ('15', '北京市', '2', '12000', '', '', '', '北京分公司', '', '', '', '1');
INSERT INTO `jzz_organization` VALUES ('16', null, '3', '12000.01', '', null, '', '部门1', null, null, null, '15');
INSERT INTO `jzz_organization` VALUES ('17', null, '3', '12000.02', '', null, '', '部门2', null, null, null, '15');
INSERT INTO `jzz_organization` VALUES ('18', null, '3', '12000.03', '', null, '', '部门3', null, null, null, '15');
INSERT INTO `jzz_organization` VALUES ('19', null, '3', '12000.04', '', null, '', '部门4', null, null, null, '15');
INSERT INTO `jzz_organization` VALUES ('20', null, '3', '12000.05', '', null, '', '部门5', null, null, null, '15');

-- ----------------------------
-- Table structure for `jzz_role`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_role`;
CREATE TABLE `jzz_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `desciption` varchar(256) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_role
-- ----------------------------

-- ----------------------------
-- Table structure for `jzz_rolemenu`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_rolemenu`;
CREATE TABLE `jzz_rolemenu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menuId` bigint(20) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l1sx4a53aig79fhch6mpuio4p` (`menuId`),
  KEY `FK_ckn4sb0ljmut4ybjj5jredpar` (`roleId`),
  CONSTRAINT `FK_ckn4sb0ljmut4ybjj5jredpar` FOREIGN KEY (`roleId`) REFERENCES `jzz_role` (`id`),
  CONSTRAINT `FK_l1sx4a53aig79fhch6mpuio4p` FOREIGN KEY (`menuId`) REFERENCES `jzz_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_rolemenu
-- ----------------------------

-- ----------------------------
-- Table structure for `jzz_rolemenubutton`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_rolemenubutton`;
CREATE TABLE `jzz_rolemenubutton` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buttonId` bigint(20) DEFAULT NULL,
  `menuId` bigint(20) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ktgj2tbl2up6l571sh4shnjw2` (`buttonId`),
  KEY `FK_1v8bvfttaw5wh63u1b86svdbe` (`menuId`),
  KEY `FK_qayaxgq2lg163vsklg4eocjd0` (`roleId`),
  CONSTRAINT `FK_1v8bvfttaw5wh63u1b86svdbe` FOREIGN KEY (`menuId`) REFERENCES `jzz_menu` (`id`),
  CONSTRAINT `FK_ktgj2tbl2up6l571sh4shnjw2` FOREIGN KEY (`buttonId`) REFERENCES `jzz_button` (`id`),
  CONSTRAINT `FK_qayaxgq2lg163vsklg4eocjd0` FOREIGN KEY (`roleId`) REFERENCES `jzz_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_rolemenubutton
-- ----------------------------

-- ----------------------------
-- Table structure for `jzz_user`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_user`;
CREATE TABLE `jzz_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birthday` date DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `deleteMark` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `gender` bit(1) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `lastVisit` datetime DEFAULT NULL,
  `logOnCount` int(11) DEFAULT NULL,
  `macAddress` varchar(255) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  `password` varchar(64) NOT NULL,
  `previousVisit` datetime DEFAULT NULL,
  `realName` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `userName` varchar(32) NOT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `departmentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tau2w35ydp3xd4d9nxj3b81e1` (`organizationId`),
  KEY `FK_hq5mw77i86whbhxffea4oc64i` (`companyId`),
  KEY `FK_1c7ulneyvgb614ig6uh6rgek6` (`departmentId`),
  CONSTRAINT `FK_1c7ulneyvgb614ig6uh6rgek6` FOREIGN KEY (`departmentId`) REFERENCES `jzz_organization` (`id`),
  CONSTRAINT `FK_hq5mw77i86whbhxffea4oc64i` FOREIGN KEY (`companyId`) REFERENCES `jzz_organization` (`id`),
  CONSTRAINT `FK_tau2w35ydp3xd4d9nxj3b81e1` FOREIGN KEY (`organizationId`) REFERENCES `jzz_organization` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_user
-- ----------------------------
INSERT INTO `jzz_user` VALUES ('1', null, '2012-06-04 01:00:00', null, null, null, null, null, null, null, null, null, null, '2012-06-04 01:00:00', '691b14d79bf0fa2215f155235df5e670b64394cc', null, 'Admin', '7efbd59d9741d34f', null, 'admin', null, null, null);

-- ----------------------------
-- Table structure for `jzz_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `jzz_user_role`;
CREATE TABLE `jzz_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tj5whpa86achfaicxjo8shii` (`roleId`),
  KEY `FK_8g3xx9dgjj07gocr2794rp03y` (`userId`),
  CONSTRAINT `FK_8g3xx9dgjj07gocr2794rp03y` FOREIGN KEY (`userId`) REFERENCES `jzz_user` (`id`),
  CONSTRAINT `FK_tj5whpa86achfaicxjo8shii` FOREIGN KEY (`roleId`) REFERENCES `jzz_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jzz_user_role
-- ----------------------------
