package ${pknEntity};

<#if baseEntityName?length gt 0>
import ${baseEntityName};
</#if>
<#if hasDate == true>
import java.util.Date;
</#if>
<#if hasBigDecimal == true>
import java.math.BigDecimal;
</#if>

public class ${className} <#if baseEntityName?length gt 0>extends ${baseEntityName?substring(baseEntityName?last_index_of(".") + 1,baseEntityName?length)}</#if>{

	<#list columns as column>
		<#if !baseEntityColumns?seq_contains(column.fieldName)>
	// ${column.comment}
    private ${column.javaType} ${column.fieldName};
		</#if>
    </#list>
	
	<#list columns as column>
	<#if !baseEntityColumns?seq_contains(column.fieldName)>
	/**
	 * @param 设置${column.comment}
	 */
    public void ${column.setMethod}(${column.javaType} ${column.fieldName}){
       	this.${column.fieldName} = ${column.fieldName};
    }
    /**
     * @return 返回${column.comment} 
     */
    public ${column.javaType} ${column.getMethod}(){
    	<#if column.javaType == 'String'>
    	return this.${column.fieldName} = ${column.fieldName} == null ? null : ${column.fieldName}.trim();
    	<#else>
    	return this.${column.fieldName};	
    	</#if>
    }
    </#if>
	</#list>

}