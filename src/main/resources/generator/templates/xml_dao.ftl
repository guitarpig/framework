<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${pknDao}.${className}Dao">
  <resultMap id="BaseResultMap" type="${pknEntity}.${className}">
  	<#list columns as column>
  	<#if column.name == 'id'>
    <id column="${column.name}" property="${column.fieldName}" />
    <#else>
    <result column="${column.name}" property="${column.fieldName}" />
    </#if>	
  	</#list>
  </resultMap>
  <sql id="Example_Where_Clause">
    <where>
      <foreach collection="oredCriteria" item="criteria" separator="or">
        <if test="criteria.valid">
          <trim prefix="(" prefixOverrides="and" suffix=")">
            <foreach collection="criteria.criteria" item="criterion">
              <choose>
                <when test="criterion.noValue">
                  AND ${r"${criterion.condition}"}
                </when>
                <when test="criterion.singleValue">
                  AND ${r"${criterion.condition} #{criterion.value}"}
                </when>
                <when test="criterion.betweenValue">
                  AND ${r"${criterion.condition} #{criterion.value} and #{criterion.secondValue}"}
                </when>
                <when test="criterion.listValue">
                  AND ${r"${criterion.condition}"}
                  <foreach close=")" collection="criterion.value" item="listItem" open="(" separator=",">
                    ${r"#{listItem}"}
                  </foreach>
                </when>
              </choose>
            </foreach>
          </trim>
        </if>
      </foreach>
    </where>
  </sql>
  <sql id="Update_By_Example_Where_Clause">
    <where>
      <foreach collection="example.oredCriteria" item="criteria" separator="or">
        <if test="criteria.valid">
          <trim prefix="(" prefixOverrides="and" suffix=")">
            <foreach collection="criteria.criteria" item="criterion">
              <choose>
                <when test="criterion.noValue">
                  AND ${r"${criterion.condition}"}
                </when>
                <when test="criterion.singleValue">
                  AND ${r"${criterion.condition} #{criterion.value}"}
                </when>
                <when test="criterion.betweenValue">
                  AND ${r"${criterion.condition} #{criterion.value} and #{criterion.secondValue}"}
                </when>
                <when test="criterion.listValue">
                  AND ${r"${criterion.condition}"}
                  <foreach close=")" collection="criterion.value" item="listItem" open="(" separator=",">
                    ${r"#{listItem}"}
                  </foreach>
                </when>
              </choose>
            </foreach>
          </trim>
        </if>
      </foreach>
    </where>
  </sql>
  <sql id="Base_Column_List">
  	<#list columns as column><#if column_index != 0>, </#if>${column.name}</#list>
  </sql>
  <select id="selectByExample" parameterType="${pknExample}.${className}Example" resultMap="BaseResultMap">
    SELECT
    <if test="distinct">
      DISTINCT
    </if>
    <include refid="Base_Column_List" />
    FROM ${tableName}
    <if test="_parameter != null">
      <include refid="Example_Where_Clause" />
    </if>
    <if test="orderByClause != null">
      ORDER BY ${r"${orderByClause}"}
    </if>
    <if test="page != null">
      LIMIT ${r"#{page.start} , #{page.end}"}
    </if>
  </select>
  <select id="selectByPrimaryKey" parameterType="java.lang.Integer" resultMap="BaseResultMap">
    SELECT 
    <include refid="Base_Column_List" />
    FROM ${tableName}
    WHERE id = ${r"#{id}"}
  </select>
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Integer">
    DELETE FROM ${tableName}
    WHERE id = ${r"#{id}"}
  </delete>
  <delete id="deleteByExample" parameterType="${pknExample}.${className}Example">
    DELETE FROM ${tableName}
    <if test="_parameter != null">
      <include refid="Example_Where_Clause" />
    </if>
  </delete>
  <insert id="insert" parameterType="${pknEntity}.${className}" useGeneratedKeys="true" keyProperty="id">
    INSERT INTO ${tableName} (
      <#list columns as column><#if column.name != "id"><#if column_index != 1>, </#if>${column.name}</#if></#list>)
    VALUES (
   	  <#list columns as column><#if column.name != "id"><#if column_index != 1>, </#if>#${"{${column.name}}"}</#if></#list>)
  </insert>
  <insert id="insertSelective" parameterType="${pknEntity}.${className}" useGeneratedKeys="true" keyProperty="id">
    INSERT INTO ${tableName}
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <#list columns as column>
      <#if column.name != "id">
      <if test="${column.name} != null">
        ${column.name},
      </if>
      </#if>
      </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
      <#list columns as column>
      <#if column.name != "id">
      <if test="${column.name} != null">
        #${"{${column.name}}"},
      </if>
      </#if>
      </#list>
    </trim>
  </insert>
  <select id="countByExample" parameterType="${pknExample}.${className}Example" resultType="java.lang.Integer">
    SELECT COUNT(*) from ${tableName}
    <if test="_parameter != null">
      <include refid="Example_Where_Clause" />
    </if>
  </select>
  <update id="updateByExampleSelective" parameterType="map">
    UPDATE ${tableName}
    <set>
      <#list columns as column>
      <if test="record.${column.name} != null">
        ${column.name} = #${"{record.${column.name}}"},
      </if>
      </#list>
    </set>
    <if test="_parameter != null">
      <include refid="Update_By_Example_Where_Clause" />
    </if>
  </update>
  <update id="updateByExample" parameterType="map">
    UPDATE ${tableName}
    SET
      <#list columns as column>
      ${column.name} = #${"{record.${column.name}}"}<#if column_index != (columns?size -1)>,</#if>
      </#list>
    <if test="_parameter != null">
      <include refid="Update_By_Example_Where_Clause" />
    </if>
  </update>
  <update id="updateByPrimaryKeySelective" parameterType="${pknEntity}.${className}">
    UPDATE ${tableName}
    <set>
      <#list columns as column>
      <#if column.name != "id">
      <if test="${column.name} != null">
        ${column.name} = #${"{${column.name}}"},
      </if>
      </#if>
      </#list>
    </set>
    WHERE id = ${r"#{id}"}
  </update>
  <update id="updateByPrimaryKey" parameterType="${pknEntity}.${className}">
    UPDATE ${tableName}
    SET 
      <#list columns as column>
      <#if column.name != "id">
      ${column.name} = #${"{${column.name}}"}<#if column_index != (columns?size -1)>,</#if>
      </#if>
      </#list>
    WHERE id = ${r"#{id}"}
  </update>
</mapper>