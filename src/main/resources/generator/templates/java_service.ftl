package ${pknService};

import ${pknEntity}.${className};
import ${pknExample}.${className}Example;
import ${pknDao}.${className}Dao;
import java.lang.Integer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.framework.common.BaseServiceSupport;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ${className}Service extends BaseServiceSupport<${className}, ${className}Example, ${className}Dao, Integer> {

	private static Logger logger = LoggerFactory.getLogger(${className}Service.class);

	@Autowired
	private ${className}Dao ${className?uncap_first}Dao;
	
	@Override
	protected ${className}Dao getDao() {
		return ${className?uncap_first}Dao;
	}
}